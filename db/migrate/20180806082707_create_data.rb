class CreateData < ActiveRecord::Migration[5.2]
  def change
    create_table :data do |t|
      t.string :nama
      t.string :email
      t.string :notelp
      t.string :alamat

      t.timestamps
    end
  end
end
